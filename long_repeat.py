import re
def long_repeat(st:"string"):
    l = []
    temp = 0
    for x in range(len(st)):
        if st[x] == st[temp]:
            s = ''
            for c in range(x, len(st)):
                if st[x] != st[c]:
                    x = c
                    break
                else:
                    s += st[c]
            l.append(s)
        if st[temp] != st[x]:
            temp = x
    return len(max(l, key=lambda s: len(s))) if l else 0
re.compile(r'^.*(.)(\1)(\1).*$')


if __name__ == '__main__':
    line= "sdsffffse"
    b = list(line)
    print(line.lstrip(b[0]))
    result = re.findall(r"Av", "aaaaavA")
    print(long_repeat(""))
    print(long_repeat('sdsffffse'))
    print(long_repeat('ddvvrwwwrggg'))
    ##These "asserts" using only for self-checking and not necessary for auto-testing
    assert long_repeat('sdsffffse') == 4
    assert long_repeat('ddvvrwwwrggg') == 3
    ## print('"Run" is good. How is "Check"?')
