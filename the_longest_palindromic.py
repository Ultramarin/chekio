def longest_palindromic(text):
    len_text = len(text)
    for j in range(len_text, 1, -1):
        for i in range(len_text-j+1):
            temp = text[i:i+j]
            if temp == temp[::-1]:
                return temp

    return text[0]


#
#print(text[::-1])
if __name__ == '__main__':
   print(longest_palindromic("artrartrt"))
   print(longest_palindromic("abacada"))
   print(longest_palindromic("aaaa") )