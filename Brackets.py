__author__ = 'Legio'
def checkio(expression):
    steck = []
    k = {")": '(', "}": "{", "]": "["}
    for x in expression:
        if x in "(){}[]":
            if x not in k:
                steck.append(x)
            else:
                if steck and k[x]!= steck[-1]:
                    return False
                elif steck:
                    steck.pop()
                else:
                    return False
#
    return False if steck else True

print(checkio("(((1+(1+1))))]"))

checkio("(((1+(1+1))))]")
print(checkio("((5+3)*2+1)"))
print(checkio("{[(3+1)+2]+}"))

print(checkio("(3+{1-1)}"))

print(checkio("[1+1]+(2*2)-{3/3}"))

print(checkio("(({[(((1)-2)+3)-3]/3}-3)"))

print(checkio("2+3"))


