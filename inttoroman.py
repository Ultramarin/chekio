__author__ = 'Legio'
tuple_numbers = tuple(zip((1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1),('M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I')))

def int_to_roman(data):
    tuple_numbers = tuple(zip((1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1),('M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I')))
    result = []
    for integer, numeral in tuple_numbers:
        count = data // integer
        result.append(numeral * count)

        data -= integer * count

    return ''.join(result)

print(int_to_roman(6))
print(int_to_roman(76))
print(int_to_roman(499))