def max2(arg, *args, key=lambda x: x):
    if args:
        iterable = [arg] + list(args)
    else:
        iterable = list(arg)
    result = iterable[0]
    for x in iterable:
        if key(x) > key(result):
            result = x
    return result

def min2(arg, *args, key=lambda x: x):
    if args:
        iterable = [arg] + list(args)
    else:
        iterable = list(arg)
    result = iterable[0]
    for x in iterable:
        if key(x) < key(result):
            result = x
    return result

