__author__ = 'Legio'
def verify_anagrams(first_word, second_word):
    first_word = first_word.lower()
    second_word = second_word.lower()
    for x in second_word:
        if second_word.count(x) > first_word.count(x) and x!=" ":
            return False

    if second_word[:] in first_word:
        return False

    return True


print(verify_anagrams("Programming", "Gram Ring Mop"))
print(verify_anagrams("Hello", "Ole Oh"))
print(verify_anagrams("Kyoto", "Tokyo"))
print(verify_anagrams("Hello", "Hell"))