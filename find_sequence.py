def checkio(matrix):
    min_len_true = 4
    len_matrix = len(matrix)
    temp_1_1 = matrix[0][0]
    temp_2_2 = matrix[0][0]
    temp_3_3 = matrix[0][0]
    temp_4_4 = matrix[0][0]
    temp_5_5 = matrix[0][0]
    temp_6_6 = matrix[0][0]
    for i in range(len_matrix):
        temp_1 = 1
        temp_2 = 1
        temp_3 = 1
        temp_4 = 1
        temp_5 = 1
        temp_6 = 1

        t = []
        for j in range(len_matrix):
            if matrix[j][i] == temp_2_2:
                temp_2 += 1
            elif matrix[j][i] != temp_2_2:
                temp_2_2 = matrix[j][i]
                temp_2 = 1
            if matrix[i][j] == temp_1_1:
                temp_1 += 1
            elif matrix[i][j] != temp_1_1:
                temp_1_1 = matrix[i][j]
                temp_1 = 1
            if (i+j) < len_matrix and matrix[i+j][j] == temp_3_3:
                temp_3 += 1
            elif (i+j) <len_matrix and matrix[i+j][j] != temp_3_3:
                temp_3_3 = matrix[i+j][j]
                temp_3 = 1
            if (i + j) < len_matrix and matrix[j][j+i] == temp_4_4:
                temp_4 += 1
            elif (i + j) < len_matrix and matrix[j][j+i] != temp_4_4:
                temp_4_4 = matrix[i][j+i]
                temp_4 = 1
            if (i + j) < len_matrix and matrix[len_matrix - (i + j) - 1][j] == temp_5_5:
                temp_5 += 1
            elif (i + j) < len_matrix and matrix[len_matrix - (i + j) - 1][j] != temp_5_5:
                temp_5_5 = matrix[len_matrix - (i + j) - 1][j]
                temp_5 = 1
            if (i + j) < len_matrix and matrix[len_matrix - j - 1][j+i] == temp_6_6:
                temp_6 += 1
            elif (i + j) < len_matrix and matrix[len_matrix -  j - 1][j+i] != temp_6_6:
                temp_6_6 = matrix[len_matrix - j - 1][j+i]
                temp_6 = 1
            if temp_1 >= 4 or temp_2 >= 4 or temp_3 >= 4 or temp_4>=4 or temp_5>=4 or temp_6>=4:
                return True

    #for x in range(len_matrix):
    #    print(matrix[x][x-1] )
#
    #for x in range(len_matrix):
    #    print(matrix[x][x-1])
#
#
    #for x in range(len_matrix):
    #    temp = []
    #    for c in range(len_matrix):
    #        if (c + x) < (len_matrix):
    #            temp.append(matrix[c+x][c])
    #    print(temp)
#
    #for x in range(len_matrix):
    #    temp = []
    #    for c in range(len_matrix):
    #        if (c + x) < (len_matrix):
    #            temp.append(matrix[len_matrix - (c + x) - 1][c])
#
    #    print(temp)
#
    #for x in range(len_matrix):
    #    temp = []
    #    for c in range(len_matrix):
    #        if (c + x) < (len_matrix):
    #            temp.append(matrix[len_matrix - c - 1][c + x])
    #    print(temp)

    #replace this for solution
    return False

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testin
    #print(checkio([
    #    [1, 1, 1, 1],
    #    [2, 1, 1, 1],
    #    [3, 4, 3, 2],
    #    [1, 1, 1, 1],
    #]))
    #print(checkio([
    #    [1, 2, 1, 1],
    #    [1, 1, 4, 1],
    #    [1, 3, 1, 6],
    #    [1, 7, 2, 5]
    #]))
    #print(checkio([
    #    [7, 1, 4, 1],
    #    [1, 2, 5, 2],
    #    [3, 4, 1, 3],
    #    [1, 1, 8, 1],
    #]))
    #print(checkio([
    #    [2, 1, 1, 6, 1],
    #    [1, 3, 2, 1, 1],
    #    [4, 1, 1, 3, 1],
    #    [5, 5, 5, 5, 5],
    #    [1, 1, 3, 1, 1]
    #]))
    #print(checkio([
    #    [7, 1, 1, 8, 1, 1],
    #    [1, 1, 7, 3, 1, 5],
    #    [2, 3, 1, 2, 5, 1],
    #    [1, 1, 1, 5, 1, 4],
    #    [4, 6, 5, 1, 3, 1],
    #    [1, 1, 9, 1, 2, 1]
    #]))
    #print(checkio([
    #    [2, 6, 2, 2, 7, 6, 5],
    #    [3, 4, 8, 7, 7, 3, 6],
    #    [6, 7, 3, 1, 2, 4, 1],
    #    [2, 5, 7, 6, 3, 2, 2],
    #    [3, 4, 3, 2, 7, 5, 6],
    #    [8, 4, 6, 5, 2, 9, 7],
    #    [5, 8, 3, 1, 3, 7, 8]
    #]))
    print( checkio([[1, 1, 7, 2, 8, 1, 9, 2, 6, 2],
                    [3, 7, 6, 6, 7, 4, 7, 5, 3, 7],
                    [2, 4, 8, 5, 3, 9, 4, 9, 4, 4],
                    [3, 1, 4, 9, 8, 9, 5, 3, 6, 8],
                    [4, 5, 6, 4, 7, 3, 9, 1, 9, 9],
                    [7, 7, 6, 4, 8, 9, 2, 3, 5, 7],
                    [9, 1, 2, 1, 1, 7, 9, 2, 6, 4],
                    [5, 7, 6, 9, 5, 3, 5, 4, 4, 9],
                    [5, 4, 7, 8, 9, 3, 1, 4, 5, 1],
                    [9, 5, 7, 9, 4, 7, 4, 5, 8, 8]]))
l = []
l.sort(reverse=True)