__author__ = 'Legio'
def checkio(data):
    col = len(data)
    row = len(data[0])
    temp = []
    transport_matrix = []
    for x in range(row):
        for c in range(col):
            temp.append(data[c][x])
        transport_matrix.append(temp)
        temp =[]
    return transport_matrix

def checkio(data):
    l = [[data[x][c] for x in range(len(data))] for c in range(len(data[0]))]
    return l

print(checkio([[1, 2, 3],
            [4, 5, 6],
                    [7, 8, 9]]))
print(checkio([[1, 4, 3],
                    [8, 2, 6],
                    [7, 8, 3],
                    [4, 9, 6],
                    [7, 8, 1]]))

