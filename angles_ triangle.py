from math import acos, degrees
import timeit

import timeit
def checkio(a,b,c):
    if a+b > c and b+c > a and c+a > b:
        return [round(degrees(acos((b**2+c**2-a**2)/(2*b*c))),1), round(degrees(acos((a**2+c**2-b**2)/(2*a*c))), 1), degrees(acos((a**2+b**2-c**2)/(2*b*a)))]
    else:
        return[0, 0, 0]

def checkio1(a,b,c):
    if a+b > c and b+c > a and c+a > b:
        l = [round(degrees(acos((b**2+c**2-a**2)/(2*b*c))),1), round(degrees(acos((a**2+c**2-b**2)/(2*a*c))), 1)]
        return l.append(180-l[0]-l[1])
    else:
        return[0, 0, 0]

print( timeit.timeit("checkio(40000000000, 40000000000, 40000000000)", setup="from __main__ import checkio", number=1))
print( timeit.timeit("checkio1(40000000000, 40000000000, 40000000000)", setup="from __main__ import checkio1", number=1))
